create table categories
(
    id            int identity
        constraint categories_pk
            primary key,
    category_name nvarchar(1000)
)


create table products
(
    id           int identity
        constraint products_pk
            primary key,
    product_name nvarchar(1000)
)


create table product_category
(
    product_id  int
        constraint productCategory_products_null_fk
            references products,
    category_id int
        constraint productCategory_categories_null_fk
            references categories
)
﻿namespace TestWork
{
    public interface IShape
    {
        double Area();
    }
}
﻿namespace TestWork
{
    public class ShapeService
    {
        public double GetAreaOfTriangle(double a, double b, double c)
        {
            return GetArea(new Triangle(a, b, c));
        }

        public double GetAreaOfCircle(double radius)
        {
            return GetArea(new Circle(radius));
        }

        public double GetArea(IShape shape)
        {
            return shape.Area();
        }
    }
}
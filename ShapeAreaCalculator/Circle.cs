﻿using System;

namespace TestWork
{
    public class Circle : IShape
    {
        private double Radius { get; }

        public Circle(double r)
        {
            Radius = r;
        }

        public double Area()
        {
            return Math.PI * Radius * Radius;
        }
    }
}
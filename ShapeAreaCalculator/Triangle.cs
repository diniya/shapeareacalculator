﻿using System;

namespace TestWork
{
    public class Triangle : IShape
    {
        private double A { get; }
        private double B { get; }
        private double C { get; }

        public Triangle(double ab, double bc, double ca)
        {
            if (ab * ab + bc * bc == ca * ca)
            {
                A = ab;
                B = bc;
                C = ca;
            }
            else if
                (ab * ab + ca * ca == bc * bc)
            {
                A = ab;
                B = ca;
                C = bc;
            }
            else
            {
                A = ca;
                B = bc;
                C = ab;
            }
        }

        public double Area()
        {
            if (IsRectangularTriangle(this))
            {
                return 0.5 * A * B;
            }

            var p = (A + B + C) / 2;
            return Math.Sqrt(p * (p - A) * (p - B) * (p - C));
        }

        private static bool IsRectangularTriangle(Triangle tr)
        {
            if (tr == null) return false;
            if (tr.A * tr.A + tr.B * tr.B == tr.C * tr.C)
            {
                return true;
            }

            return false;
        }
    }
}
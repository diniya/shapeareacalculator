﻿using System;
using NUnit.Framework;

namespace TestWork.Tests
{
    /// <summary>
    /// tests.
    /// </summary>
    [TestFixture]
    public class ShapeServiceTest
    {
        private ShapeService _service;

        [SetUp]
        public void ShapeServiceTestSetup()
        {
            _service = new ShapeService();
            
        }

        /// <summary>
        /// test GetOfTriangle.
        /// </summary>
        [Test]
        [TestCase(3, 5, 4, 6)]
        [TestCase(1, 1, 2, 0)]
        [TestCase(29, 20, 21, 210)]
        public void GetOfTriangleIsDefined(double a, double b, double c, double expectedArea)
        {
            var area = _service.GetAreaOfTriangle(a, b, c);
            Assert.AreEqual(expectedArea, area);
        }

        /// <summary>
        /// test GetAreaOfCircle.
        /// </summary>
        [Test]
        [TestCase(2, Math.PI * 2 * 2)]
        [TestCase(2.1, Math.PI * 2.1 * 2.1)]
        [TestCase(3, Math.PI * 3 * 3)]
        public void GetAreaOfCircleIsDefined(double r, double expectedArea)
        {
            var area = _service.GetAreaOfCircle(r);
            Assert.AreEqual(expectedArea, area);
        }
        
        /// <summary>
        /// test GetArea.
        /// </summary>
        [Test]
        public void GetAreaIsDefined()
        {
            var shape = new Circle(4);
            var area = _service.GetArea(shape);
            Assert.AreEqual(Math.PI*4*4 , area);
        }
    }
}